import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home.vue'
import ImgIndex from '@/components/imgComponents/ImgIndex.vue'
import ImgResult from '@/components/imgComponents/ImgResult.vue'
import ImgRandom from '@/components/imgComponents/ImgRandom.vue'
import Login from '@/components/loginComponents/Login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
	{
	  path: '/login',
	  name: 'Login',
	  component: Login
	},
	{
		path: '/imgIndex',
		name: 'ImgIndex',
		component: ImgIndex,
		children: [
			{
				path: '/result/:keyWord',
				name: 'ImgResult',
				component: ImgResult
			},
			{
				path: '/random',
				name:  'ImgRandom',
				component: ImgRandom
			}
		]
	}
  ]
})
