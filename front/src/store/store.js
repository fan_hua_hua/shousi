import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store =  new Vuex.Store({
		state: {
			showImage: false,
				currentImg: {}, //当前选中图片
			},
			mutations: {
				setCurrentImg(state, newImg){
					state.currentImg = newImg;
				},
				changeShowImage(state){
					state.showImage = !state.showImage;
				}
			},
			getters:{
				getCurrentImg(state){
					return state.currentImg;
				},
				getShowImage(state){
					return state.showImage;
				}
			}
});
export default store;