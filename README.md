# shousi

#### 介绍
图片站点，学习用~所用图片均来自于网络，如有侵权立删致歉！由于本人目前学习重心在前端，后端还有待学习后重构，望见谅！

#### 软件架构
nodejs(koa2)+vue2(vue-cli+vuex+vue-router)+mysql+python(为了爬取数据，以提供sql文件)


#### 使用说明

前端 front目录
1. npm install或cnpm install
后端 backend目录
1. npm run start（暂时先如此，等重构后再做修改）
数据库 data目录
1. 新建数据库shousi
2. source命令导入数据，sql文件为data目录下的shousi.sql
爬虫
1. 用sql文件导数据就不用再爬取了，都是入门级别的爬虫，感兴趣可以看看~


#### 声明

1.  所用图片均来自于网络，如有侵权立删致歉！
