import pandas as pd


# 数据转为csv
def img_data_to_csv(data, headers, path):
    # headers = ['src', 'tags', 'colors', 'size', 'outer_net', 'src_site']
    test = pd.DataFrame(data, columns=headers)
    test.to_csv(path, index=False, encoding='utf-8')
