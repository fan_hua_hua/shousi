import jieba
import jieba.analyse as analyse


# 使用jieba库对文本进行关键词提取
def get_tags(string):
    string = string.strip('图片')
    result = [i[0] for i in jieba.analyse.extract_tags(string, topK=10, withWeight=True)]
    return result

