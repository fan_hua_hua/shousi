import requests

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/89.0.4389.128 Safari/537.36',
    'referer': 'https://pixabay.com/',
    'cookie': '__cfduid=df102d43d09db6fc6647b9cadea12285d1618290662; '
              'csrftoken=v5kmU4thIIsOM22IrbpHuHnz8qsYUimWpUEfCxPC7h1FoikRzDWfQO3splK1aJlo; lang=zh; '
              '_ga=GA1.2.1661492930.1618290663; _gid=GA1.2.1938792974.1618290663; is_human=1; '
              'dwf_photo_stats_async=True; dwf_use_comment_recaptcha=False; '
              'anonymous_user_id=6a024700-b4fe-4775-8e42-2117e6519084; '
              '__cf_bm=08de114d55dfdd2ecaf429678711feae1ee67e3c-1618450108-1800-AeMr250g9aPTU4H1KKOQ3DRKYi'
              '/zZwD1eoGZh1skQiL8KxdKp9O8kfX4QN6P0VSPJoUhf+GghyYA14UsV1fbnBA=; client_width=169',
}

session = requests.session()

min_page = 1
max_page = 190
baseURL = 'https://pixabay.com/zh/images/search/?order=ec&'

img_data_list = []

# 源站点
src_site = 'pixabay'

for i in range(1, 2):
    r = session.request('GET', baseURL+'pagi='+str(i), headers=headers)
    print(r.text)
    # soup = BeautifulSoup(r.text, 'html.parser')
    # img_tags = soup.select('.photo-result-image')
    # print(img_tags)
    # for img_tag in img_tags:
    #     img_data = {}
    #     img_data['src_site'] = src_site
    #     print(img_tag['alt'])
