const router = require('koa-router')()
const User = require('../models/user')
const Sequelize = require("sequelize");
const mySequelize = require("../models/mySequelize")

router.prefix('/users')

// 随机返回一张图片
router.get('/login', async (ctx, next)=>{
    let ctx_query = ctx.query;
    let user = await User.findOne({
        where:{
            userName: ctx_query['userName']
        }
    });
    let res = {}
    if(user === null){
        res['code'] = '301';
        res['msg'] = '用户名错误';
    }
    else if(user['password']!==ctx_query['password']){
        res['code'] = '301';
        res['msg'] = '密码错误';
    }else{
        res['code'] = '200';
        res['msg'] = '登入成功';
    }
    ctx.body = JSON.stringify(res)
})

module.exports = router
