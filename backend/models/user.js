const Sequelize = require("sequelize");
const sequelize = require("./mySequelize");

const User = sequelize.define('user_table', {
    userId: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    userName: Sequelize.STRING(100),
    password: Sequelize.STRING(150),
}, {
    timestamps: false,
    freezeTableName: true
});
module.exports = User;