const Sequelize = require("sequelize");
const sequelize = require("./mySequelize");

const Image = sequelize.define('img_table', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    src: Sequelize.STRING(250),
    visit: Sequelize.INTEGER,
    like: Sequelize.INTEGER,
    colors: Sequelize.STRING(100),
    size: Sequelize.STRING(20),
    upload_time: Sequelize.DATE,
    outer_net: Sequelize.INTEGER,
    src_site: Sequelize.STRING(100),
    tags: Sequelize.STRING(150)
}, {
    timestamps: false,
    freezeTableName: true
});
module.exports = Image;